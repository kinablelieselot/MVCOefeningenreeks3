﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Oefeningenreeks3.Models;
using Oefeningenreeks3;

namespace Oefeningenreeks3.Controllers
{
    public class BandLidController : Controller
    {
        private List<Band> bands;
  
        public BandLidController()
        {
            bands = BandInitializer.StartUp();
        }
    
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Lijst()
        {
            return View(bands);
        }

        [HttpGet]
        public IActionResult Maak(string bandlidnaam, int bandlidjaar, Geslacht geslacht, string bandnaam)
        {
            ViewBag.bandnaam = bandnaam;
            return View(new BandLid()
                    { Naam=bandlidnaam, Leeftijd=bandlidjaar, Geslacht=geslacht}
                );
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Maak(BandLid bandLid, string band)
        {
            if (ModelState.IsValid)
            {
                bands.Add(
                     new Band()
                     {
                         Naam = band,
                         Leden = new List<BandLid>()
                            { new BandLid()
                                { Naam = bandLid.Naam, Geslacht = bandLid.Geslacht, Leeftijd= bandLid.Leeftijd, Levend = true }
                            }
                     });
                return View("Lijst", bands);
            }
            return View(bandLid);
        }

    }
}
